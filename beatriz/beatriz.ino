/*
Beatriz
*/

#include <TMRpcm.h>
#include <SPI.h>
#include <SD.h>

File root;
int sensorPin = 3;
int sdPin = 4;
int outPin = 9;
int previousState;
TMRpcm tmrpcm;

void setup()
{
  Serial.begin(115200);
  pinMode(sensorPin,INPUT);
  tmrpcm.speakerPin = outPin;
  previousState = digitalRead(sensorPin);
  
  String position = previousState == 1 ? "dow" : "up";  
  Serial.println(position);
  
  if (!SD.begin(sdPin)) {
    Serial.println("initialization failed!");
    return;
  }else{
    root = SD.open("/");
    Serial.println("initialization done.");
  }
}

void loop()
{
  int sensorState =  digitalRead(sensorPin);
  if(sensorState == 0 && previousState != sensorState){
    Serial.println("start");    
    playAllMusic(root);
   }else if(sensorState == 1 && previousState != sensorState){
    Serial.println("stop");
    stopAllMusic();
   }
   previousState = sensorState;
   delay(500);
}

void stopAllMusic(){
  tmrpcm.stopPlayback();
}

void playAllMusic(File dir) {
  //long time = 0; 
  if (dir.isDirectory()){
     while(true){
     //time = 0;
     File entry =  dir.openNextFile();
     if(!entry){
       Serial.println("empty folder");
       dir.rewindDirectory();
       break;
     }
     if(entry.isDirectory()){
      playAllMusic(entry);
     }else{
       Serial.println(entry.name());
       playMusic(entry);
       //time = millis();
       //Serial.println(time);
       //delay(time);
     }
    }
  }
}

void playMusic(File music){
  String name = String(music.name());
  name.toLowerCase();
  if(name.endsWith(".wav")){
     if(!tmrpcm.isPlaying()){
        String msg = "playing... " + name;
        Serial.println(msg);
        tmrpcm.volume(99999);
        tmrpcm.play(music.name());
        //music.close();
     }else{
       String msg2 =  "music is already playing";
       Serial.println(msg2);
     }  
  }else{
    Serial.println("it is not a wav file");
  }
}

